package graphs;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import gens.Foule;
import gens.Personnel;
import utilitaire.Coords;

public abstract class PathFinder {
	protected Graph graph;
	
	public PathFinder(Graph graph) {
		this.graph = graph;
	}
	
	public abstract ArrayList<Vertex> findShortestPath(Vertex v1, Vertex v2);
	
	public Graph getGraph() {
		return this.graph;
	}
	
	/**
	 * Permet de tracer un chemin al�atoire dans l'interface graphique afin d'examiner son fonctionnement.
	 * 
	 * @param g2d interface graphique o� tracer le chemin.
	 */
	public static void tracerTestChemin(Graphics2D g2d) {
		ArrayList<Vertex> path = Foule.calculerChemin(new Personnel(42, Coords.randCoords(), 10.0, 10.0, new Color(50)), null);
		ArrayList<Line2D> polyline = new ArrayList<Line2D>();
		for (int i=0; i<path.size()-1; i++) {
			polyline.add(new Line2D.Double(path.get(i).coords.x, path.get(i).coords.y, path.get(i+1).coords.x, path.get(i+1).coords.y));
		}
		g2d.setStroke(new BasicStroke(7));		//pour tracer en plus gros
		g2d.setColor(Color.RED);
		for (Line2D ligne : polyline) {g2d.draw(ligne);}
		g2d.setStroke(new BasicStroke(1));		//mais on veut pas TOUT tracer en plus gros non plus, alors on enl�ve
	}
}
