package graphs;

import java.util.ArrayList;

import utilitaire.Coords;

public class Vertex extends Graph {
	//private String id;
	public Coords coords;
	public ArrayList<Edge> in;
	public ArrayList<Edge> out;
	
	public Vertex(Coords coords) {
		//this.id = id;
		this.coords = coords;
		this.in = new ArrayList<Edge>();
		this.out = new ArrayList<Edge>();
	}
	
	public Coords getCoords() {
		return this.coords;
	}
	
	public ArrayList<Edge> getIn() {
		return this.in;
	}
	
	public ArrayList<Edge> getOut() {
		return this.out;
	}	
	
	public void addIn(Edge e) {
		in.add(e);
	}
	
	public void addOut(Edge e) {
		out.add(e);
	}
	
	public ArrayList<Vertex> successors() {
		ArrayList<Vertex> succ = new ArrayList<Vertex>();
		for(Edge e : out) {
			if (!succ.contains(e.getEnd())) {
				succ.add(e.getEnd());
			}
		}	
		return succ;
	}
	
	public ArrayList<Vertex> predecessors() {
		ArrayList<Vertex> pred = new ArrayList<Vertex>();
		for(Edge e : in) {
			if (!pred.contains(e.getStart())) {
				pred.add(e.getStart());
			}
		}
		return pred;
	}
	
	public int degree() {
		return predecessors().size() + successors().size();
	}
	
	
	@Override
	public String toString() {
		return "V" + coords;
	}
	
	@Override
	public boolean equals(Object o) {
		Vertex v = (Vertex) o;
		return this.coords.equals(v.coords);
	}
}
