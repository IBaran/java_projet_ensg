package graphs;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;

import obstacleFixe.ChargementGrilles;
import utilitaire.Coords;

public class Graph {

	//Attributs_______________________________________________________________________________________________
	private ArrayList<Vertex> vertices;
	private ArrayList<Edge> edges;
	private ArrayList<Rectangle2D.Double> listeObstaclesSansPortes =  ChargementGrilles.obs.listeObstaclesSansPortes;


	//Constructeur____________________________________________________________________________________________
	public Graph() {
		vertices = new ArrayList<Vertex>();
		edges = new ArrayList<Edge>();
	}

	//Getters_________________________________________________________________________________________________
	public ArrayList<Vertex> getVertices() {
		return this.vertices;
	}

	public ArrayList<Edge> getEdges() {
		return this.edges;
	}

	//M�thodes________________________________________________________________________________________________
	/**
	 * Calcule le nombre de Vertex du graph.
	 * 
	 * @return le nombre de Vertex du graph.
	 */
	public int numberOfVertices() {
		return vertices.size();
	}

	/**
	 * Calcule le nombre d'Edges du graph.
	 * 
	 * @return le nombre d'Edges du graph.
	 */
	public int numberOfEdges() {
		return edges.size();
	}

	/**
	 * Ajoute un vertex au graph.
	 * 
	 * @param v le vertex � ajouter.
	 */
	public void addVertex(Vertex v) {
		if (!vertices.contains(v)) {
			vertices.add(v);
		}
	}

	/**
	 * Cr�er un edge unidirectionnel de start vers end, et l'ajoute au graph.
	 * 
	 * @param edgeWeight poids de l'edge.
	 * @param start vertex de d�part de l'edge.
	 * @param end vertex d'arriv�e de l'edge.
	 */
	public void addEdge(double edgeWeight, Vertex start, Vertex end) {
		Edge e = new Edge(edgeWeight, start, end);
		if (vertices.contains(start) && vertices.contains(end) && !edges.contains(e)) {
			edges.add(e);
			start.addOut(e);
			end.addIn(e);
		}
	}

	/**
	 * Ajoute un edge unidirectionnel au graph.
	 * 
	 * @param e edge � ajouter au graph.
	 */
	public void addEdge(Edge e) {
		if (vertices.contains(e.start) && vertices.contains(e.end) && !edges.contains(e)) {
			edges.add(e);
			e.start.addOut(e);
			e.end.addIn(e);
		}
	}

	/**
	 * Cr�er un edge bidirectionnel, et l'ajoute au graphjoute un edge bidirectionnel au graph.
	 * ie ajour deux edges : un de start vers end et un de end vers start.
	 * 
	 * @param edgeWeight poids de l'edge.
	 * @param start vertex de d�part de l'edge.
	 * @param end vertex d'arriv�e de l'edge.
	 */
	public void addUndirectedEdge(double edgeWeight, Vertex start, Vertex end) {
		this.addEdge(edgeWeight, start, end);
		this.addEdge(edgeWeight, end, start);
	}

	/**
	 * Ajoute un (ou deux) Vertex temporaire au graph, et le(s) relie aux autres noeuds accessibles du graph.
	 *
	 * @param start vertex de d�part de l'edge.
	 * @param end vertex d'arriv�e de l'edge.
	 */
	public void addVertexTemp(Vertex start, Vertex end) {
		//start
		this.addVertex(start);												//ajoute le vertex start
		for (Vertex v : this.getVertices()) {								//le relie aux autres vertex accessibles
			if (v.equals(start)) {continue;}
			else if (trajectoireValide(v.coords, start.coords, listeObstaclesSansPortes)) {
				double dist = v.coords.dist(start.coords);
				Edge e = new Edge(dist, start, v);
				this.addEdge(e);
			}
		}

		//end
		this.addVertex(end);												//ajoute le vertex end
		for (Vertex v : this.getVertices()) {								//le relie aux autres vertex accessibles
			if (trajectoireValide(end.coords, v.coords, listeObstaclesSansPortes) && !v.equals(end)) {
				double dist = v.coords.dist(end.coords);
				Edge e = new Edge(dist, v, end);
				this.addEdge(e);
			}
		}
	}

	/**
	 * Supprime les Vertex temporaire cr�� par la m�thode pr�c�dente.
	 * 
	 * @param start vertex de d�part de l'edge.
	 * @param end vertex d'arriv�e de l'edge.
	 */
	public void removeVertexTemp(Vertex start, Vertex end) {
		//end
		for (int i=0; i<=end.degree()-1; i++) {								//pour chaque edge connect� � end
			Edge e = this.getEdges().get(getEdges().size()-1);
			Vertex vConnect = e.oppositeExtremity(end);
			vConnect.getOut().remove(vConnect.getOut().size()-1);			//supprimer la trace de l'edge
			this.getEdges().remove(this.getEdges().size() -1);				//supprimer l'edge
		}
		this.getVertices().remove(this.getVertices().size() -1);			//suppression du vertex end

		//start
		for (int i=0; i<start.degree()-1; i++) {							//pour chaque edge connect� � start
			Edge e = this.getEdges().get(getEdges().size()-1);
			Vertex vConnect = e.oppositeExtremity(start);
			vConnect.getIn().remove(vConnect.getIn().size() -1);			//supprimer la trace de l'edge
			this.getEdges().remove(this.getEdges().size() -1);				//supprimer l'edge
		}
		this.getVertices().remove(this.getVertices().size() -1);			//suppression du vertex start
	}

	/**
	 * D�tect les coordonn�es des angles des obstacles.
	 * 
	 * @param grille matrice d'entiers repr�sentant les diff�rents obstacles.
	 */
	public static ArrayList<Coords> detectPointInteret(int[][] grille)  {
		ArrayList<Coords> listeCoords = new ArrayList<Coords>();
		int n = grille.length;
		int m = grille[0].length;

		for (int i=0 ; i<n ; i++) {
			for (int j=0 ; j<m ; j++) {
				if (grille[i][j] == 0) {
					Coords c = new Coords(i,j);
					ArrayList<Coords> coordsPotentiel = new ArrayList<Coords>();
					coordsPotentiel.addAll(Arrays.asList(new Coords(c.x-1,c.y), new Coords(c.x,c.y+1), new Coords(c.x+1,c.y), new Coords(c.x,c.y-1)));

					// si il s'agit d'un angle d'obstacle : 
					if (i==0 || j==0 || i==n-1 ) {continue;}
					else if (grille[i-1][j-1]!=0 && grille[i][j-1]==0 && grille[i-1][j]==0) {
						listeCoords.add(new Coords(i+1,m-(j+1+1)));					//bas gauche
					}
					else if (grille[i-1][j+1]!=0 && grille[i][j+1]==0 && grille[i-1][j]==0) {
						listeCoords.add(new Coords(i+1,m-(j+1-1))); 				//haut gauche
					}
					else if (grille[i+1][j-1]!=0 && grille[i][j-1]==0 && grille[i+1][j]==0) {
						listeCoords.add(new Coords(i-1,m-(j+1+1)));					//bas droite
					}
					else if (grille[i+1][j+1]!=0 && grille[i][j+1]==0 && grille[i+1][j]==0) {	//haut droite
						listeCoords.add(new Coords(i-1,m-(j+1-1)));
					}
				}
			}
		}
		return listeCoords;
	}

	/**
	 * D�termine si une trajectoire (droite) entre deux points traverse un obstacle ou non.
	 * 
	 * @param c1 coordonn�es de d�parts.
	 * @param c2 coordonn�es d'arriv�es.
	 * @param listeObstacles liste des obstacles de la grille en type Rectangle2D.
	 * @return true si la trajectoire est valide, ie ne rencontre aucune obstacle, false sinon.
	 */
	public static boolean trajectoireValide(Coords c1, Coords c2, ArrayList<Rectangle2D.Double> listeObstacles) {
		Line2D.Double line = new Line2D.Double(c1.x, c1.y, c2.x, c2.y);
		for (Rectangle2D.Double obstacle : listeObstacles) {
			if (line.intersects(obstacle)) {return false;}
		}
		return true;
	}

	/**
	 * Cr�er un graph optimis� � partir d'une grille :
	 * chaque angle d'obstacle est repr�sent� par un noeud.
	 * 
	 * @param grille matrice d'entiers repr�sentant les diff�rents obstacles.
	 * @param listeObstaclesSansPortes liste des obstacles de la grille (sans les portes) en type Rectangle2D.
	 * @return le graph cr��.
	 */
	public static Graph creerGraphOpti(int[][] grille, ArrayList<Rectangle2D.Double> listeObstaclesSansPortes) {
		Graph graph = new Graph();

		//Ajout des vertex
		for (Coords c : detectPointInteret(grille)) { 
			graph.addVertex(new Vertex(c.toPlanCoords()));
		}
		/*
		graph.addVertex(new Vertex(new Coords(0, 165).toPlanCoords()));		//porte1
		graph.addVertex(new Vertex(new Coords(0, 465).toPlanCoords()));		//porte2
		graph.addVertex(new Vertex(new Coords(1190, 315).toPlanCoords())); 	//porte3
		 */
		//Ajout des edges
		for (Vertex v : graph.getVertices()) {
			for (Vertex vtest : graph.getVertices()) {
				if (vtest.equals(v)) {continue;}
				else if (trajectoireValide(v.coords, vtest.coords, listeObstaclesSansPortes)) {
					double dist = v.coords.dist(vtest.coords);
					graph.addEdge(dist, v, vtest);
				}
			}
		}

		return graph;
	}
	
	/**
	 * Trace les noeuds et les arcs du graph dans l'interface graphique afin de le visualiser.
	 * 
	 * @param g2d interface graphique o� tracer le graph.
	 */
	public void tracerGraph(Graphics2D g2d) {
		g2d.setColor(new Color(150,150,255));
		for (Edge e : this.getEdges()) {				//trac� edges
			Line2D lineuh = new Line2D.Double(e.getStart().coords.x, e.getStart().coords.y, e.getEnd().coords.x, e.getEnd().coords.y);
			g2d.draw(lineuh);
		}
		g2d.setColor(new Color(0,0,200));
		for (Vertex v : this.getVertices()) {			//trac� vertex
			double i = v.coords.x;
			double j = v.coords.y;
			Rectangle2D.Double noeud2 = new Rectangle2D.Double(i-2, j-2, 4, 4);
			g2d.fill(noeud2);
		}
	}
}