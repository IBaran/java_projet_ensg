package graphs;

import java.util.ArrayList;
import java.util.HashMap;

public class DijkstraPathFinder extends PathFinder {
	public DijkstraPathFinder(Graph graph) {
		super(graph);
	}
	
	private ArrayList<Vertex> visitedVertices = new ArrayList<Vertex>();
	private HashMap<Vertex, Vertex> predecessor = new HashMap<Vertex, Vertex>();
	private HashMap<Vertex, Double> distancesMin = new HashMap<Vertex, Double>();
	private ArrayList<Vertex> sp = new ArrayList<Vertex>();
	
	public ArrayList<Vertex> findShortestPath(Vertex start, Vertex end) {
		if (start.equals(end)) {
			System.out.println("ERREUR PATHFINDING : L'arriv�e est identique � l'entr�e");
			return null;
		}
		if (!(this.getGraph().getVertices().contains(start) && this.getGraph().getVertices().contains(end))) {
			System.out.println("ERREUR PATHFINDING : L'entr�e ou l'arriv�e du chemin n'appartient pas au graph.");
			return null;
			}
		for (Vertex v : this.graph.getVertices()) {
			distancesMin.put(v,Double.POSITIVE_INFINITY);
		}
		distancesMin.put(start , (double) 0);
		
		//tant que tous les sommets n'ont pas �t� visit�s
		while(visitedVertices.size() != this.graph.getVertices().size()) {
			Vertex current = null;
			double dmin = Double.POSITIVE_INFINITY;
			
			// selectionne le sommet non visite, visitable, le moins eloigne de current
			for (Vertex v : distancesMin.keySet()) {
				if (!visitedVertices.contains(v)) {
					if (distancesMin.get(v) < dmin) {
						dmin = distancesMin.get(v);
						current = v;
					}
				}
			}
			visitedVertices.add(current);
			
			// si c'est pas la fin, on continue
			if (current != end) {
				for (Edge e : current.getOut()) {
					double distance = distancesMin.get(current) + e.getWeight();
					Vertex successor = e.oppositeExtremity(current);
					//System.out.println("current : " + current + "        getIn : " + current.getIn().size() + "         getOut : " + current.getOut().size());
					//System.out.println("\nstart : " + start + "        end : " + end);
					//System.out.println("current : " + current + "      successor : " + successor + "             " + graph.getVertices().contains(successor) + "  " + graph.getEdges().contains(e) + "    " + current.getOut().size());
					if (distance < distancesMin.get(successor)) {
						distancesMin.put(successor, distance);
						predecessor.put(successor,  current);
					}
				}
			}
		}
		sp = new ArrayList<Vertex>();
		if (predecessor.containsKey(end)) {
			Vertex current = end;
			while (predecessor.containsKey(current)) {
				sp.add(0, current);
				current = predecessor.get(current);
			}
			sp.add(0,start);
		}
		
		return sp;
	}
}