package graphs;

public class Edge extends Graph {
	double weight;
	Vertex start;
	Vertex end;
	
	public Edge(double weight, Vertex start, Vertex end) {
		//this.id = id;
		this.weight = weight;
		this.start = start;
		this.end = end;
	}
	
	public double getWeight() {
		return this.weight;
	}
	
	public Vertex getStart() {
		return this.start;
	}
	
	public Vertex getEnd() {
		return this.end;
	}
	
	public Vertex oppositeExtremity(Vertex v) {
		if (v.equals(this.start)) {
			return this.end;
		}
		else if (v.equals(this.end)) {
			return this.start;
		}
		else {
			return null;
		}
	}
	
	@Override
	public String toString() {
		return "Edge : (" + start.getCoords() + ", " + end.getCoords() + ", " + weight + ")";
	}

}
