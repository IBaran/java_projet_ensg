package obstacleFixe;

import javax.swing.JComponent;


public abstract class Obstacle extends JComponent {
	private static final long serialVersionUID = -2578815018219126046L;

	//attributs
	public double taille;

	public Obstacle(double taille) {
		this.taille = taille;
	}

}