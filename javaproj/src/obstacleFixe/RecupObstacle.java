package obstacleFixe;

import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class RecupObstacle {
	public ArrayList<Rectangle2D.Double> listeObstacles;
	public ArrayList<Rectangle2D.Double> listeObstaclesSansPortes;
	public int[][] grille = new int[120][63];
	public int largeur;
	public int longueur;
	
	/**
	   * R�cup�re les information d'un fichier texte mod�lisant une grille d'obstacles
	   * 
	   * @param chemin du fichier � ouvrir, grille dans laquelle sera contenue les informations du ficheir
	   */
	public RecupObstacle(String chemin, int[][] grille){
		//Lecture d'un fichier
		// chemin absolu vers le fichier
		String filePath = chemin;
		Path p = Paths.get(filePath);
		
		//creation d'une liste des obstacles de type rectangle
		listeObstacles = new ArrayList<Rectangle2D.Double>();
		listeObstaclesSansPortes = new ArrayList<Rectangle2D.Double>();
		int taille = 10;
	    
		//ouverture d'un buffer en lectuer
		try(BufferedReader reader = Files.newBufferedReader(p)){
			String line;
			int j = 0;
			while ((line = reader.readLine()) != null) {
				
				String[] parts = line.split(" ");
				this.largeur = parts.length;
				
				 int i = 0;
				 for (String b:parts) {
					 grille[j][i] = Integer.parseInt(b);

					 //ajout de l'obstacle � la liste
					 if (grille[j][i] != 0) {
							Rectangle2D.Double caseMur = new Rectangle2D.Double(j*taille, (largeur-1-i)*taille, taille, taille);
							if (grille[j][i] != 3) {listeObstaclesSansPortes.add(caseMur);}	//on ne consid�re pas les portes pour la sortie des gens
							listeObstacles.add(caseMur);	//on ne consid�re pas les portes pour la sortie des gens
							}
					 i++;
			         }
				 j++;
			}
			this.longueur = j;
			//System.out.println("listeObs : " + listeObstacles.size() + "           sansPorte : " + listeObstaclesSansPortes.size());
			//System.out.println("larg = " + largeur + "   long = " + longueur);
		}
		catch(IOException e) {e.printStackTrace();}
		this.grille = grille;
	}
}