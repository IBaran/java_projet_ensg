package obstacleFixe;

public class ChargementGrilles {
	public static RecupObstacle obsPorte1   = new RecupObstacle("annexe\\obstacle_ouverte_1.txt", new int[120][63]);
	public static RecupObstacle obsPorte2   = new RecupObstacle("annexe\\obstacle_ouverte_2.txt", new int[120][63]);
	public static RecupObstacle obsPorte3   = new RecupObstacle("annexe\\obstacle_ouverte_3.txt", new int[120][63]);
	public static RecupObstacle obsPorte12  = new RecupObstacle("annexe\\obstacle_ouverte_1_2.txt", new int[120][63]);
	public static RecupObstacle obsPorte13  = new RecupObstacle("annexe\\obstacle_ouverte_1_3.txt", new int[120][63]);
	public static RecupObstacle obsPorte23  = new RecupObstacle("annexe\\obstacle_ouverte_2_3.txt", new int[120][63]);
	public static RecupObstacle obsPorte123 = new RecupObstacle("annexe\\obstacles_porte_ouverte.txt", new int[120][63]);
	public static RecupObstacle obs 		 = new RecupObstacle("annexe\\obstacles.txt", new int[120][63]);

	public static int longueur = obs.grille.length;   
	public static int largeur  = obs.grille[0].length;
}
