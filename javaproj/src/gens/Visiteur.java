package gens;

import java.awt.Color;

import utilitaire.Coords;

public class Visiteur extends Personne {
	private static final long serialVersionUID = 1L;
	
	//Constructeur_______________________________________________________________________________________________
	public Visiteur(int age, Coords position, double vitesse , double taille, Color color) {
		super((int) (10 + Math.random() * (70)), position, vitesse, taille, color);
	}
	
	
	//M�thodes override__________________________________________________________________________________________
	@Override
	public double vitesseAleatoire() {
		return getVitesse(); 
	}
	
	@Override
	public double vitesseAlarme() {
		if (getAge() < 60) {
		return getVitesse() * 1.5 ; 
		}
		else {
			return getVitesse() * 1.1;
		}
	}
}
