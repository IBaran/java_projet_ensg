package gens;

import java.awt.Color;

import utilitaire.Coords;

public class Intervenant extends Personne{
	private static final long serialVersionUID = -4714907597246173904L;


	//Constructeur_______________________________________________________________________________________________
	public Intervenant(int age, Coords position, double vitesse, double taille, Color color) {
		super((int) (20 + Math.random() * (50)), position, vitesse, taille, color);
	}
	

	//M�thodes override__________________________________________________________________________________________
	@Override
	public double vitesseAlarme() {
		if (getAge() < 60) {
		return getVitesse() * 1.5 ; 
		}
		else {
			return getVitesse();
		}
	}

	@Override
	public double vitesseAleatoire() {
		return getVitesse();
	}
}
