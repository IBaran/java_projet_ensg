package gens;

import java.awt.Color;
import java.util.ArrayList;

import utilitaire.Coords;

public class Etudiant extends Personne {
	private static final long serialVersionUID = 1L;

	//Attributs__________________________________________________________________________________________________
	ArrayList<Coords> interCoords = Coords.intervenantCoords();
	

	//Constructeur_______________________________________________________________________________________________
	public Etudiant(int age, Coords position, double vitesse, double taille, Color color) {
		super((int) (18 + Math.random() * (20)), position, vitesse, taille, color);
	}

	
	//M�thodes override__________________________________________________________________________________________
	@Override
	public double vitesseAleatoire() {
		for(Coords inter : interCoords) {
			if (inter.dist(getPosition()) < 20 ) {
				if (getVitesse() > 0.5) {
					setVitesse(getVitesse() * 0.8);
				}
			}
			else {
				if (getVitesse() < 2)
				setVitesse(getVitesse() * 1.05);
			}
		}
		return getVitesse();
	}
	
	@Override
	public double vitesseAlarme() {
		return getVitesse() * 1.5 ; 
	}
}
