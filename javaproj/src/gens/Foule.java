package gens;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import graphs.DijkstraPathFinder;
import graphs.Graph;
import graphs.PathFinder;
import graphs.Vertex;
import obstacleFixe.ChargementGrilles;
import utilitaire.Coords;

public class Foule{
	private ArrayList<Personne> listePersonnes;
	private double vitesse;
	private double taille;
	private int age;
	public static Graph graph = Graph.creerGraphOpti(ChargementGrilles.obs.grille, ChargementGrilles.obs.listeObstaclesSansPortes);
	public static PathFinder finder = new DijkstraPathFinder(graph);
	public HashMap<Personne, ArrayList<Vertex>> destinations;

	/**
	 * Constructeur pour cr�er une foule
	 * 
	 * @param nombre de sous personnes de chaque classe dans la foule 
	 */
	public Foule(int nbEtudiant, int nbPersonnel, int nbVisiteur, int nbIntervenant) {
		nbIntervenant = 24;
		
		this.listePersonnes = new  ArrayList<Personne>(); 
		creerIndividus(nbEtudiant, nbPersonnel, nbVisiteur, nbIntervenant);

		if (!(nbEtudiant ==0 && nbPersonnel ==0 && nbVisiteur==0 && nbIntervenant==0)) {
			destinations = initRandDestination();
		}
	}

	public Foule(ArrayList<Personne> listePersonnes) {
		this.setListePersonnes(listePersonnes);
	}

	/**
	 *Ajoute une personne � la foule 
	 *
	 *@param personne qu'on veut ajouter dans la foule
	 */
	public void add(Personne personne) {
		this.getListePersonnes().add(personne);
	}

	/**
	 *Enl�ve une personne � la foule 
	 *
	 *@param personne qu'on veut retirer de la foule
	 */
	public void remove(Personne personne) {
		this.getListePersonnes().remove(personne);
	}

	/**
	 * Cr�e n individus d'un certain type dans la foule
	 * 
	 * @param nombres d'individus que l'on doit cr�er.
	 */
	public void creerIndividus(int nbEtudiant, int nbPersonnel, int nbVisiteur, int nbIntervenant) {
		ArrayList<Coords> listeCoords = new ArrayList<Coords>(Coords.intervenantCoords().subList(0, nbIntervenant));
		Coords.randCoords(nbEtudiant + nbPersonnel + nbVisiteur, listeCoords);

		for (int i=0; i<listeCoords.size(); i++) {
			if (i<nbIntervenant) {
				listePersonnes.add(new Intervenant(age , listeCoords.get(i), vitesse, taille, new Color(136,  77, 167)));
			}
			else if (i>=nbIntervenant && i<nbIntervenant + nbEtudiant) {
				listePersonnes.add(new Etudiant(age , listeCoords.get(i), vitesse, taille, new Color(116, 208, 241)));
			}
			else if (i>=nbIntervenant + nbEtudiant && i<nbIntervenant + nbEtudiant + nbPersonnel) {
				listePersonnes.add(new Personnel(age , listeCoords.get(i), vitesse, taille, new Color(174,  74, 52)));
			}
			else if (i>=nbIntervenant + nbEtudiant + nbPersonnel) {
				listePersonnes.add(new Visiteur(age , listeCoords.get(i), vitesse, taille, new Color(1, 215, 88)));
			}
		}
	}

	/**
	 * Associe une destination al�atoire � chaque personne de la foule, 
	 * ainsi que le chemin pour y parvenir (sous forme de pile de coordonn�es).
	 * 
	 * @return le dictionnaire de Personne et chemin.
	 */
	public HashMap<Personne, ArrayList<Vertex>> initRandDestination() {
		HashMap<Personne, ArrayList<Vertex>> destinations = new HashMap<Personne, ArrayList<Vertex>>();
		for (Personne p : this.getListePersonnes()) {
			ArrayList<Vertex> path = calculerChemin(p, null);
			//System.out.println("taille chemin : " + path.size() + "   [l.109]");
			destinations.put(p, path);
		}
		return destinations;
	}

	//D�placements_______________________________________________________
	/**
	 * Calcule le chemin � emprunter pour qu'une personne atteigne sa destination destination.
	 * 
	 * @param personne cherchant � se d�placer.
	 * @return pile de vertex constituant le chemin.
	 */
	public static ArrayList<Vertex> calculerChemin(Personne p, Coords endCoords) {
		Vertex start = new Vertex(p.getPosition());
		Vertex end   = new Vertex(endCoords);

		for (Vertex v : graph.getVertices()) {
			if (v.equals(end)) {end = v;}
		}

		if (endCoords == null) {end = new Vertex(Coords.randCoords());}

		//Ajout temporaire de deux noeuds : la personne et sa destination
		//if (graph.getVertices().contains(end)) { graph.addVertexTemp(start, null); }
		graph.addVertexTemp(start, end);

		//Calcul du chemin
		PathFinder finder = new DijkstraPathFinder(graph);
		ArrayList<Vertex> path = finder.findShortestPath(start, end);

		//Suppression des deux noeuds temporaires
		//if (graph.getVertices().contains(end)) {graph.removeVertexTemp(start, null);}
		graph.removeVertexTemp(start, end);

		return path;
	}

	/**
	 * Effectue les d�placements de chaque personne dans la foule en temps normal (1 it�ration).
	 * 
	 * @param destinations dictionnary of persons and the path to reach their destination.
	 */
	public void deplacerRandDestination(HashMap<Personne, ArrayList<Vertex>> destinations) {
		//� ajouter : les personnes s'arrettent un peu une fois arriv� � leur destination, avant de repartir
		for (Personne p : this.getListePersonnes()) {
			if (! (p instanceof Intervenant)) {
				p.setVitesse(p.vitesseAleatoire()); 

				if (p.getPosition().dist(destinations.get(p).get(0).coords) < 10) {		//on d�pile le chemin
					if (destinations.get(p).size() != 1 ) {
						destinations.get(p).remove(0);  
					}
					else {
						ArrayList<Vertex> path = calculerChemin(p, null);				//on racalcule un nouvel itin�raire al�atoire
						destinations.put(p, path);
					}
				}
				p.deplacement(destinations.get(p).get(0).coords, this);
			}
		}
	}

	/**
	 * Effectue les d�placements de chaque personne dans la foule lors de l'�vacuation (1 it�ration).
	 * 
	 * @param destinations dictionnary of persons and the path to reach their destination.
	 * @param d_open liste des �tats des portes (true = ouverte).
	 * @param d_coords liste des coordonn�es des portes de sortie.
	 */
	public void deplacerEvacuation(List<Boolean> d_open, List<Boolean> d_open_before, List<Coords> d_coords) {
		ArrayList<Personne> listePersonnesCopie = new ArrayList<Personne>(this.listePersonnes); //pour �viter de modifier la liste sur laquelle on it�re

		//test si les portes ont �t� modifi�es
		boolean doorUpdated = false;
		for (int i=0; i<d_open.size(); i++) {if (d_open.get(i) != d_open_before.get(i)) {doorUpdated = true;}}
		if (doorUpdated) { 				//on met � jour les chemins d'�vacuation des gens
			updateEvacuation(d_open, d_coords);
		}

		for (Personne p : listePersonnesCopie) {
			if (destinations.get(p).get(destinations.get(p).size()-1).coords.dist(p.getPosition()) < 20 && (d_open.get(0) || d_open.get(1) || d_open.get(2))) {
				this.remove(p);
			}
			if (p.getPosition().dist(destinations.get(p).get(0).coords) < 10) {	//on d�pile le chemin
				if (destinations.get(p).size() != 1 ) {
					destinations.get(p).remove(0); 
				}
			}
			p.deplacement(destinations.get(p).get(0).coords, this);
		}
	}

	public void updateEvacuation(List<Boolean> d_open, List<Coords> d_coords) {
		for (Personne p : listePersonnes) {			//recalcule de la porte la plus proche
			Coords endCoords = p.getOut(d_coords, d_open);
			ArrayList<Vertex> path = calculerChemin(p, endCoords);
			destinations.put(p, path);
		}
	}

	//Getters et setters________________________________________________________
	public ArrayList<Personne> getListePersonnes() {
		return listePersonnes;
	}

	public void setListePersonnes(ArrayList<Personne> listePersonnes) {
		this.listePersonnes = listePersonnes;
	}
}