package gens;

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.swing.JComponent;

import obstacleFixe.ChargementGrilles;
import utilitaire.Coords;
import utilitaire.Vecteur;


public abstract class Personne extends JComponent {
	private static final long serialVersionUID = 982341623875029585L;
	
	//Attributs_______________________________________________________________________________________________
	private int age;
	private Coords position;
	private double vitesse;
	private double taille;
	private Color color; 
	private Ellipse2D.Double circle;

	//Constructeur____________________________________________________________________________________________
	public Personne(int age, Coords position, double vitesse, double taille, Color color) {
		this.setAge(age);
		this.setPosition(position);
		this.setVitesse(Math.random() * 2 + 2) ;
		this.setTaille(Math.random() * 3 + 7);
		this.setColor(color); 
		this.setCircle(new Ellipse2D.Double(position.x, position.y, taille, taille));
	}


	/**
	 * D�termine la vitesse des personnes pendant la phase de deplacement al�atoire.
	 * 
	 * @return vitesse d'une personne pendant la phase al�atoire.
	 */
	public abstract double vitesseAleatoire();

	/**
	 * D�termine la vitesse des personnes pendant la phase de deplacement avec alarme.
	 * 
	 * @return vitesse d'une personne pendant la phase de deplacement connue.
	 */
	public abstract double vitesseAlarme();


	// M�thodes de g�om�trie fondamentale __________________________________________________________________
	/**
	 * Calcule le carr� d'un nombre.
	 * 
	 * @param nombre dont on veut connaitre le carr�.
	 * @return carr� du nombre nombre.
	 */
	public static double toSquare(double nb) {
		return nb*nb;
	}

	/**
	 * Calcule le centre d'un cercle de type Ellipse2D.Double.
	 * 
	 * @param cercle dont ont souhaite calculer le centre.
	 * @return coordonn�es du centre du cercle.
	 */  
	public static Coords getCenter(Ellipse2D.Double circle) {
		double cx = circle.x + circle.width/2;
		double cy = circle.y + circle.width/2;
		return new Coords(cx,cy);
	}


	/**
	 * Calcule le centre d'un rectangle de type Rectangle2D.Double.
	 * 
	 * @param rectangle dont ont souhaite calculer le centre.
	 * @return coordonn�es du centre du rectangle.
	 */
	public static Coords getCenter(Rectangle2D.Double rect) {
		double cx = rect.x + rect.width/2;
		double cy = rect.y + rect.height/2;
		return new Coords(cx,cy);
	}

	/**
	 * D�termine si un cercle entre en collision avec un rectangle.
	 * 
	 * @param circle le cercle � tester.
	 * @param rectangle le rectalngle � tester.
	 * @return true si une ellipse rentre en collision avec un rectangle, false sinon.
	 */  
	public static boolean intersects(Ellipse2D.Double circle, Rectangle2D.Double rect) {
		//ecart entre les deux centres des figures (permet de tra�ter les 4 cadrants en un seul)
		double dx = Math.abs(getCenter(circle).x - getCenter(rect).x);
		double dy = Math.abs(getCenter(circle).y - getCenter(rect).y);

		//the circle is obviously far enough
		if (dx > (rect.width/2 + circle.width/2))  { return false; } //rayon = width/2
		if (dy > (rect.height/2 + circle.width/2)) { return false; }

		//the circle is obviously crossing the rectangle
		if (dx <= (rect.width/2))  { return true; } 
		if (dy <= (rect.height/2)) { return true; }

		//difficult case for the corner of the rectangle
		double cornerDistance_sq = toSquare(dx - rect.width/2) + toSquare(dy - rect.height/2);
		return (cornerDistance_sq <= toSquare(circle.width/2));
	}

	/**
	 * D�termine si deux cercles entrent en collision.
	 * 
	 * @param circle1 1er cercle � tester.
	 * @param circle2 2nd cercle � tester.
	 * @return true si deux ellipses rentrent en collision, false sinon.
	 */
	public static boolean intersectsCircle(Ellipse2D.Double circle1, Ellipse2D.Double circle2) {
		Coords coords1 = getCenter(circle1);
		Coords coords2 = getCenter(circle2);
		return coords1.dist(coords2) < circle1.width + circle2.width;
	}


	/**
	 * D�termine si une ellipse entre en collision avec un obstacle de la grille.
	 * 
	 * @param ellipse � tester.
	 * @return true si deux ellipses rentrent en collision, false sinon.
	 */
	public static boolean intersects(Ellipse2D.Double circle) {
		for (Rectangle2D.Double obstacle : ChargementGrilles.obs.listeObstacles) {
			if (intersects(circle, obstacle)) {return true;}
		}
		return false;
	}


	// M�thodes s'appliquant aux Personne ____________________________________________________________________
	/**
	 * D�termine si deux personnes entrent en collision.
	 * 
	 * @param personne � tester.
	 * @return true si il y a une collision, false sinon.
	 */
	public boolean intersects(Personne p) {
		return intersectsCircle(this.getCircle() , p.getCircle());
	}

	/**
	 * Effectue le d�placement d'une personne (1 it�ration).
	 * Si aucun d�placement n'est possible, la personne est bloqu�e et ne peut pas se d�placer.
	 * 
	 * @param destination le dictionnaire des chemins � parcourir par personne de la foule.
	 * @param foule la foule r�pertoriant tous les individus.
	 */
	public void deplacement(Coords destination, Foule foule) {
		Vecteur vectDirr = new Vecteur(destination.x - getPosition().x, destination.y - getPosition().y);
		vectDirr.normalize();

		//position apr�s le d�placement potentiel
		Coords newCoords = deplacementPossible(vectDirr, foule);
		if ( !newCoords.equals(new Coords(-1,-1)) ) {		//il existe un d�placement possible
			getPosition().x = newCoords.x;
			getPosition().y = newCoords.y;
		}
		else {} 									   		//la personne est bloqu�e
	}

	/**
	 * D�termine si la personne peut se d�placer, et o�.
	 * En priorit� vers sa destination, puis en s'en �loignant si c'est impossible.
	 * 
	 * @param vectDirr vecteur directeur normalis� de la personne vers sa destination.
	 * @param foule la foule r�pertoriant tous les individus.
	 * @return les coordonn�es de l'emplacement possible o� la personne peut se d�placer, des coordonn�es absurdes sinon.
	 */
	public Coords deplacementPossible(Vecteur vectDirr, Foule foule) {
		for (double theta : Arrays.asList((double) 0, Math.PI/4, -Math.PI/4, Math.PI/2, -Math.PI/2, 3*Math.PI/4, -3*Math.PI/4, Math.PI)) {
			Vecteur vectDirrAlt = vectDirr.rotation(theta);
			Vecteur coordsAlt = new Vecteur();
			coordsAlt.x = getPosition().x + vectDirrAlt.x * this.getVitesse();
			coordsAlt.y = getPosition().y + vectDirrAlt.y * this.getVitesse();
			Ellipse2D.Double positionAlt = new Ellipse2D.Double(coordsAlt.x, coordsAlt.y, this.getTaille(), this.getTaille());

			boolean intersection = false;
			if (intersects(positionAlt)) 						{intersection = true;} 		//collision avec le d�cor
			else if (this.intersectionGens(positionAlt, foule)) {intersection = true;}		//collision avec une personne

			if (!intersection /*|| this.intersectionGens(this.circle, foule)*/) { 		//d�s que l'une des directions est possible, on y va
				return new Coords(coordsAlt.x, coordsAlt.y);
			}
			if (intersection && Math.random()*20<1) {new Coords(-1, -1);}					//si intersection, une chance sur 20 de s'arr�ter
		}
		return new Coords(-1, -1);		//sinon, on renvoit des coordonn�s absurdes pour signifier qu'elle est bloqu�
	}

	/**
	 * Teste si la position alternative induira une collision avec une personne de la foule.
	 * Suivant le principe du Sweep and Prune.
	 * 
	 * @param positionAlt la position que tiendra la personne si elle se d�place.
	 * @param foule la foule r�pertoriant tous les individus.
	 * @return true si la position alternative induit une collision, false sinon.
	 */
	public boolean intersectionGens(Ellipse2D.Double positionAlt, Foule foule) {
		ArrayList<Personne> listePersonnes = new ArrayList<Personne>();
		double x1 = positionAlt.x;

		//Sweep : on balaye la liste compl�te pour selectionner les collisions potentielles
		for (Personne p2 : foule.getListePersonnes()) {
			double x2 = p2.getPosition().x;
			if (x2 + p2.getTaille() > x1  &&  x2 < x1 + this.getTaille() && !this.equals(p2)) {
				listePersonnes.add(p2);
			}
		}
		//Prune : on test si la collision potentielle est une vraie collision
		for (Personne p2 : listePersonnes) {
			if (intersectsCircle(positionAlt, p2.getCircle())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Identifie la sortie la plus proche de la personne.
	 * 
	 * @param d_coords liste des coordonn�es des portes.
	 * @param d_open liste des �tats des portes (true = ouverte).
	 * @return coordonn�es de la sortie la plus proche.
	 */	
	public Coords getOut(List<Coords> d_coords, List<Boolean> d_open) {
		Coords coords = this.getPosition();
		List<Boolean> d_open_local = new ArrayList<Boolean>(d_open);
		//dist des portes � la personne		
		List<Double> d_dists  = Arrays.asList(coords.dist(d_coords.get(0)), coords.dist(d_coords.get(1)), coords.dist(d_coords.get(2)));

		if (!d_open.get(0) && !d_open.get(1) && !d_open.get(2)) {
			//si tout est ferm�, les gens vont quand m�me vers la plus proche
			Collections.fill(d_open_local, Boolean.TRUE);
		}

		//dico des sorties possibles et leur distance
		HashMap<Double, Coords> dist = new HashMap<Double, Coords>();
		for (int i=0; i<d_open_local.size(); i++) {
			if (d_open_local.get(i)) {
				dist.put(d_dists.get(i), d_coords.get(i)); 
			}
		}
		return dist.get(Collections.min(dist.keySet()));
	}

	// Getters / setters__________________________________________________________________
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Coords getPosition() {
		return position;
	}

	public void setPosition(Coords position) {
		this.position = position;
	}

	public double getTaille() {
		return taille;
	}

	public void setTaille(double taille) {
		this.taille = taille;
	}

	public double getVitesse() {
		return vitesse;
	}

	public void setVitesse(double vitesse) {
		this.vitesse = vitesse;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}


	public Ellipse2D.Double getCircle() {
		return circle;
	}


	public void setCircle(Ellipse2D.Double circle) {
		this.circle = circle;
	}


	//M�thodes override___________________________________________________________________
	public String toString() {
		return  this.getClass() +  " ;  �ge : " + this.getAge() + " ;  vitesse : " + this.getVitesse() + " ;  position : " + this.getPosition() + ".";
	}

	public boolean equals(Personne p) {
		return this.getPosition().equals(p.getPosition()) && this.getTaille() == p.getTaille();
	}
}