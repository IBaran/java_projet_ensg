package utilitaire;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import obstacleFixe.ChargementGrilles;
import obstacleFixe.RecupObstacle;

public class Coords extends Vecteur{
	  public Coords(double x, double y){
		  super(x,y);
	  }
	  
	  public Coords(Coords c) {
		  super(c.x, c.y);
	  }
	  
	  
	  // methodes de calculs ________________________________________________________________  
	  /**
	   * Calcule la distance entre deux points.
	   * 
	   * @param coordonn�es d'un point c
	   * @return distance entre 2 points
	   */
	  public double dist(Coords c) {
		  return Math.sqrt( (c.x - this.x)*(c.x - this.x) + (c.y - this.y)*(c.y - this.y) );
	  }
	  
	  /**
	   * Convertis les coordonn�es planes en coordonn�es de la grille.
	   * 
	   * @return les coordonn�es du point dans la grille
	   */
	  public Coords toGridCoords() {
		  int taillePix = 10;
		  int largeur = 63;
		  int dy = (int) ((this.x)/taillePix) ;
		  int dx = (int) (largeur-(this.y)/taillePix) ;
		  return new Coords(dx, dy);
	  }
	  
	  /**
	   * Convertis les coordonn�es de la grille en coordonn�es planes.
	   * 
	   * @return le centre des "cases"
	   */
	  public Coords toPlanCoords() {
		  return new Coords(this.x*10+5, this.y*10+5);
	  }
	  
	  
	  // methodes de test sur les coordonn�es ________________________________________________________________
	  /**
	   * Teste si les coordonn�es sont trop pr�s de celles d�j� g�n�r�es.
	   * 
	   * @param liste des coordonn�es valides o� une personne peut se trouver
	   * @return booleen :
	   * - true si le point est trop pr�s des points d�j� g�n�r�s;
	   * - false sinon
	   */
	  public boolean coordsIntersectsGens(ArrayList<Coords> listeCoords) {
	      for (Coords c : listeCoords) {
	    	  if (this.dist(c) < 21) {
	    		  return true;
	    	  } 
	      }
	      return false;
	  }
	  
	  /**
	   * Teste si les coordonn�es sont dans un ObstacleFixe ou non.
	   * 
	   * @return booleen :
	   * - true si les coordonn�es sont dans un ObstacleFixe;
	   * - false sinon
	   */
	  public boolean coordsValides() {
		    RecupObstacle obs = ChargementGrilles.obs;  
		    int lon = ChargementGrilles.longueur - 1;
			int lar = ChargementGrilles.largeur - 1;
		  	
			int taille = 10;		//taille d'une case
		    int dy = (int) (this.x/taille) ;
		    int dx = (int) (this.y/taille) ;
		    //si le centre du cercle est dans un obstacle
		    if (obs.grille[dy][dx]!=0 ) {
		    	return false; 
		    }
		    if (dx!=0 && dy!=0 && dx!=lar && dy!=lon) {
		    	if (obs.grille[dy-1][lar-dx  ]!=0 || obs.grille[dy-1][lar-dx-1]!=0 || obs.grille[dy-1][lar-dx+1]!=0 ||
		    		obs.grille[dy+1][lar-dx  ]!=0 || obs.grille[dy+1][lar-dx-1]!=0 || obs.grille[dy+1][lar-dx+1]!=0 || 
		    		obs.grille[dy  ][lar-dx  ]!=0 || obs.grille[dy  ][lar-dx-1]!=0 || obs.grille[dy  ][lar-dx+1]!=0) {
		    		return false;
		    	}
		    }
		    return true;
	  }	 
	  
	  // methodes de g�n�ration de coordonn�es ________________________________________________________________	  
	  /**
	   * G�n�re les Coordonn�es d'une position al�atoire o� un individu peut appara�tre.
	   * 
	   * @return coordonn�es d'une position al�atoire o� un individu peut appara�tre
	   */
	  public static Coords randCoords() {
		  int longueur = ChargementGrilles.longueur;
		  int largeur  = ChargementGrilles.largeur;
		  
		  Coords coords = new Coords(Math.random()*((longueur-1)*10),Math.random()*((largeur-1)*10));
		  while (!coords.coordsValides()) {  		//Tant qu'ils sont trop pr�s d'un obstacle.
			  coords =    new Coords(Math.random()*((longueur-1)*10),Math.random()*((largeur-1)*10)); 
			  }
	  	  return coords;
	  }
	  
	  /**
	   * G�n�re les Coordonn�es de n positions al�atoires o� un individu peut appara�tre.
	   * 
	   * @param entier n : nombre de positions al�atoires voulues 
	   * @return coordonn�es de n positions al�atoires o� un individu peut appara�tre
	   */
	  public static ArrayList<Coords> randCoords(int n, ArrayList<Coords> listeCoords) {
		  if (listeCoords == null) {listeCoords = new ArrayList<Coords>();}
		  for (int i=0 ; i<n ; i++) {
			  Coords coords = randCoords();
			  //Tant qu'ils sont trop pr�s d'autres coordonn�es.
			  while (coords.coordsIntersectsGens(listeCoords)) {
				  coords = randCoords();
			  }
			  listeCoords.add(coords);
		  }
	  	  return listeCoords;
	  }
	  
	  /**
	   * G�n�re la liste des 24 positions des intervenants pr�s de leur stand
	   * 
	   *  @return liste des coordonn�es des intervenants
	   */

	  public static ArrayList<Coords> intervenantCoords(){
			//Lecture d'un fichier
			String filePath = "annexe\\coord_intervenants.txt" ;
			Path p = Paths.get(filePath);
			
			//creation d'une liste de coordonn�es
			ArrayList<Coords> listeCoords = new ArrayList<Coords>();
			
			//ouverture d'un buffer en lectuer
			try(BufferedReader reader = Files.newBufferedReader(p)){
				String line;

				while ((line = reader.readLine()) != null) {
					String[] parts =  line.split(" ");
					//ajout dans la liste
    				 listeCoords.add(new Coords(Integer.parseInt(parts[0]) , Integer.parseInt(parts[1])));
				         }
				}		
			catch(IOException e) {e.printStackTrace();}
			return listeCoords;
		}
	  
}
	
	  
	