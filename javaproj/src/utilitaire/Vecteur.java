package utilitaire;

public class Vecteur {
	public double x;
	public double y;
	
	//constructeur 
	public Vecteur() {
		this.x = 0;
		this.y = 0;
	}
	
	public Vecteur(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Calcule la norme d'un vecteur
	 * 
	 * @return norme d'un vecteur
	 */
    public double getNorm() {
        return Math.sqrt (x * x + y * y);
    }
	
	/**
	 * Normalise un vecteur
	 * 
	 * @return vecteur normalis�
	 */
	public void normalize() {
		if (this.x != 0 || this.y != 0) {
			double norm = this.getNorm();
			this.x = this.x / norm;
			this.y = this.y / norm;
		}
	}
	
	/**
	 * Calcule la rotation d'angle theta d'un vecteur
	 * 
	 * @param theta : angle de la rotation
	 * @return vecteur rotationn� d'un angle theta
	 */
	public Vecteur rotation(double theta) {
		double x2 = Math.cos(theta)*this.x - Math.sin(theta)*this.y;
		double y2 = Math.sin(theta)*this.x + Math.cos(theta)*this.y;
		
		return new Vecteur(x2, y2);
	}
	
	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}
	
	/**
	 * Compare la valeur de deux instances
	 * 
	 * @param objet � comparer
	 * @return booleen
	 * - true si les deux instances ont la m�me valeur;
	 * - false sinon
	 */
	public boolean equals(Object o) {
		Vecteur vect = (Vecteur) o;
		if (vect == null) {return false;}
		return this.x == vect.x && this.y == vect.y;
	}	
	
}
