package fenetre;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JMenuBar; 

public class Fenetre extends JFrame  {
	private static final long serialVersionUID = 7706906385605426304L;
	ConteneurFenetre pan; 
	JMenuBar menu; 
	
	public Fenetre() {
		super("Simulation de Foule");
		this.proprieteFenetre(); 
	}
	
	public void proprieteFenetre() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		//r�cupere la dimension de l'�cran
		Dimension tailleMoniteur = Toolkit.getDefaultToolkit().getScreenSize();
		int longueur = tailleMoniteur.width;
		int hauteur = tailleMoniteur.height;
		this.setSize(longueur, hauteur);
		this.setLocationRelativeTo(null);
		this.setResizable(false);

		pan = new ConteneurFenetre();
		this.setContentPane(pan);
		
		menu =  pan.Menu();
		this.setJMenuBar(menu);
	}
}
	
