package fenetre;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import gens.Foule;
import gens.Personne;
import graphs.Graph;
import obstacleFixe.ChargementGrilles;
import obstacleFixe.RecupObstacle;
import utilitaire.Coords;

public class ConteneurFenetre extends JPanel {
	private static final long serialVersionUID = -4203230666795640577L;
	private Foule foule; 

	private JButton boutonSimulation; 
	private JButton boutonPersonne; 
	private JButton boutonAlarme; 
	private JButton boutonReinitialiser; 
	private JButton boutonPause;
	
	private JMenuItem menuLancementAlarme;
	private JMenuItem menuLancementSimualtion;
	private JMenuItem menuPause;
	private JMenuItem menuValiderPersonne; 

	private JCheckBox boitePorte1; 
	private JCheckBox boitePorte2; 
	private JCheckBox boitePorte3; 

	private JCheckBoxMenuItem boiteMenuPorte1;
	private JCheckBoxMenuItem boiteMenuPorte2;
	private JCheckBoxMenuItem boiteMenuPorte3;; 

	private JTextField champEtudiant; 
	private JTextField champPersonnel; 
	private JTextField champVisiteur; 
	private JTextField champIntervenant; 

	private JLabel etiquetteTemps;

	private boolean activationSimulation = false;  
	private boolean activationAlarme = false; 
	private boolean initialisation; 
	private boolean reinitialisation = false; 
	private boolean pause = false; 
	private boolean fenetreOption = true; 

	private int nombreEtudiant; 
	private int nombrePersonnel; 
	private int nombreVisiteur;
	private int nombreIntervenant;

	private int time; 

	//private HashMap<Personne, ArrayList<Vertex>> destinations; 

	Dimension tailleMoniteur = Toolkit.getDefaultToolkit().getScreenSize();
	int longueur = tailleMoniteur.width;
	int hauteur = tailleMoniteur.height;
	
	private Graph graph = Graph.creerGraphOpti(ChargementGrilles.obs.grille, ChargementGrilles.obs.listeObstaclesSansPortes);


	//__________________________SIMULATEUR_____________________________________________________________//
	//_________________________________________________________________________________________________//
	public ConteneurFenetre() {
		super();
		this.proprieteConteneur();
	}

	/**
	 * Affiche le contenu du conteneur de la fen�tre
	 */
	public void proprieteConteneur() {
		this.setLayout(null);
		this.proprietesRaccourci();
		this.proprietesLegende1();
		this.proprietesEtiquetteTemps();
	}

	/**
	 * Affiche le temps de la simulation
	 */
	private void proprietesEtiquetteTemps() {	
		JLabel etiquetteTempsSimulation = new JLabel("Dur�e de l'�vacuation (en secondes) : ");
		etiquetteTempsSimulation.setBounds(10, 10, 240, 20);
		this.add(etiquetteTempsSimulation);

		etiquetteTemps = new JLabel("0");
		etiquetteTemps.setBounds(240, 10, 200, 20);
		this.add(etiquetteTemps);
	}

	/**
	 * Affiche les raccourcis clavier sur la fenetre de la simualtion
	 */	
	private void proprietesRaccourci() {	
		String x = "<div style=\"text-align: left;\">"
				+ "alt + o : Options de la simulation&nbsp;</div><div style=\"text-align: left;\">"
				+ "alt + p : Simulation en pause/en fonctionnement&nbsp;</div><div style=\"text-align: left;\">"
				+ "alt + a : D�clenchement de l'alarme</div>";
		JLabel textx = new JLabel("<html>" + x +"</html>"); 
		textx.setBounds(5, 620, 300, 100);
		this.add(textx);
		
		String y = "<div style=\"text-align: left;\">"
				+ "alt + s : Lancement de la simulation&nbsp;</div><div style=\"text-align: left;\">"
				+ "alt + h : Manuel de la simulation et aide&nbsp;</div><div style=\"text-align: left;\">"
				+ "alt + r : R�initialisation</div>";
		JLabel texty = new JLabel("<html>" + y +"</html>"); 
		texty.setBounds(305, 620, 250, 100);
		this.add(texty);
		
		String z = "<div style=\"text-align: left;\">"
				+ "alt + 1 : changer l'�tat de la porte 1 (en haut � gauche)</div><div style=\"text-align: left;\">"
				+ "alt + 2 : changer l'�tat de la porte 2 (en bas � gauche)</div><div style=\"text-align: left;\">"
				+ "alt + 3 : changer l'�tat de la porte 3 (� droite)</div>";
		JLabel textz = new JLabel("<html>" + z +"</html>"); 
		textz.setBounds(555, 620, 400, 100);
		this.add(textz);
	}

	/**
	 * Affiche une legende sur la fenetre de la simualtion
	 */	
	private void proprietesLegende1() {	
		ImageIcon legende = new ImageIcon(new ImageIcon("annexe\\legende.png").getImage().getScaledInstance(longueur - 1000, (longueur - 1000)*21/100 , Image.SCALE_DEFAULT));
		JLabel legende1  = new JLabel( legende, SwingConstants.HORIZONTAL)	;
		legende1.setBounds(longueur - 480, 650, longueur - 1000, hauteur - 720);
		add(legende1);
	}

	//__________________________MENU___________________________________________________________________//
	//_________________________________________________________________________________________________//
	/**
	 * Retourne une barre de menu et ses propri�t�s
	 * 
	 * @return menuBar : barre de menu qui permet d'acceder aux diff�rents param�tres de la simulation
	 */
	public JMenuBar Menu() {
		JMenuBar menuBar = new JMenuBar(); 
		this.proprietesFile(menuBar);
		this.proprietesEdit(menuBar);
		this.proprietesHelp(menuBar);	
		return menuBar;
	}

	/**
	 * D�finition de la propri�t� "File" du menu 
	 */
	public void proprietesFile(JMenuBar menuBar) {
		JMenu menuFile = new JMenu( "File" );
		menuBar.add(menuFile);

		JMenuItem menuNouvelleSimulation = new JMenuItem( "Nouvelle simulation" );
		menuFile.add(menuNouvelleSimulation);
		menuNouvelleSimulation.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.ALT_MASK));
		menuNouvelleSimulation.addActionListener(new ActionReinitialisation());
	}

	/**
	 * D�finition de la propri�t� "Options" du menu 
	 */
	public void proprietesEdit(JMenuBar menuBar) {
		JMenu menuEdit = new JMenu( "Options" );
		menuBar.add(menuEdit);

		JMenuItem menuOptions = new JMenuItem( "Options de la simulation" );
		menuEdit.add(menuOptions);
		menuOptions.addActionListener(new ActionCliqueParametre());	
		menuOptions.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
		
		menuEdit.addSeparator();

		menuValiderPersonne = new JMenuItem( "Valider le nombre de personnes" );
		menuEdit.add(menuValiderPersonne);
		menuValiderPersonne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.ALT_MASK));
		menuValiderPersonne.addActionListener(new ActionNombrePersonne());

		menuLancementSimualtion = new JMenuItem( "Lancement de la simulation" );
		menuEdit.add(menuLancementSimualtion);
		menuLancementSimualtion.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.ALT_MASK));
		menuLancementSimualtion.addActionListener(new ActionBoutonSimulation());	
		menuLancementSimualtion.setEnabled(false);

		menuLancementAlarme = new JMenuItem( "	Lancement de l'alarme" );
		menuEdit.add(menuLancementAlarme);
		menuLancementAlarme.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
		menuLancementAlarme.addActionListener(new ActionAlarme());	
		menuLancementAlarme.setEnabled(false);

		menuPause = new JMenuItem( "Mettre en pause/marche" );
		menuEdit.add(menuPause);
		menuPause.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.ALT_MASK));
		menuPause.addActionListener(new ActionCliquePause());
		menuPause.setEnabled(false);

		//______________________________________________________________________________________________________
		JMenu gestionPorte = new JMenu("Gestion des portes");
		menuEdit.add(gestionPorte);

		boiteMenuPorte1 = new JCheckBoxMenuItem("porte1");
		boiteMenuPorte2 = new JCheckBoxMenuItem("porte2");
		boiteMenuPorte3 = new JCheckBoxMenuItem("porte3");

		gestionPorte.add(boiteMenuPorte1);
		gestionPorte.add(boiteMenuPorte2);
		gestionPorte.add(boiteMenuPorte3);

		boiteMenuPorte1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));
		boiteMenuPorte1.addActionListener(new ActionPorte1());

		boiteMenuPorte2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));
		boiteMenuPorte2.addActionListener(new ActionPorte2());

		boiteMenuPorte3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.ALT_MASK));
		boiteMenuPorte3.addActionListener(new ActionPorte3());
	}

	/**
	 * D�finition la propri�t� "Help" du menu 
	 */
	public void proprietesHelp(JMenuBar menuBar) {
		JMenu menuHelp = new JMenu( "Help" );
		menuBar.add(menuHelp);

		JMenuItem menuAide = new JMenuItem( "Aide" );
		menuHelp.add(menuAide);
		menuAide.addActionListener(new ActionCliqueHelp());	
		menuAide.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.ALT_MASK));
	}	


	//_________________________ACTION DE L'UTILISATEUR_________________________________________________//
	//_____________________et m�thodes associ�es_______________________________________________________//

	//************************************************************************************************//
	/**
	 * Lire les lignes d'un fichier txt 
	 * 
	 * @param nom du fichier txt � lire 
	 * @return lignes du fichier txt
	 */
	public String readFile( String file ){
		String lines = "";
		String line;
		try {
			BufferedReader reader = new BufferedReader( new FileReader(file) );
			while ((line = reader.readLine()) != null ) {
				lines += line+"\n";
			}
		}	
		catch( Exception e ) {
			lines = "Une erreur s'est produite durant la lecture du flux : " + e.getMessage();
		}  
		return lines;
	}

	public class ActionCliqueHelp implements ActionListener {
		@Override
		/**
		 * Ouverture d'une fenetre avec l'aide pour le fonctionnement de la simulation 
		 */
		public void actionPerformed(ActionEvent arg0){
			JFrame fenetreAide = new JFrame();
			fenetreAide.setTitle("Aide : simulation de foule");
			fenetreAide.setSize(1300, 400);
			fenetreAide.setLocationRelativeTo(null);
			fenetreAide.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			fenetreAide.setVisible(true);

			JTextArea text = new JTextArea(readFile("annexe\\aide_simulation.txt"));
			fenetreAide.add(text);
			text.setEditable(false);
		}
	}

	//************************************************************************************************//	
	/**
	 * Permet de :
	 * - de cr�er les diff�rents champs de textes utiles et de d�finir leurs propri�t�s
	 * - de les ajouter au conteneur
	 * - �ventuellement, que l'utilisateur puisse interagir avec
	 * 
	 * @return ContentPaneText : Conteneur contenant les champs de textes que l'utilisateur pourra utiliser 
	 */
	private JPanel proprietesTextField() {
		JPanel ContentPaneText = new JPanel (new GridLayout(4, 1, 0, 10));

		//1. Champ de texte pour le nombre d'�tudiants	
		champEtudiant = new JTextField("10");
		champEtudiant.setPreferredSize(new Dimension(30, 20));
		ContentPaneText.add(champEtudiant);	

		//2.  Champ de texte  pour le nombre de personnels
		champPersonnel = new JTextField("10");
		ContentPaneText.add(champPersonnel);	

		//3.  Champ de texte  pour le nombre de visiteurs
		champVisiteur = new JTextField("10");
		ContentPaneText.add(champVisiteur);	

		//4.  Champ de texte pour le nombre d'intervanants
		champIntervenant = new JTextField("24");
		champIntervenant.setEnabled(false);
		ContentPaneText.add(champIntervenant);	
		return ContentPaneText;
	}

	/**
	 * Permet :
	 * - de cr�er les diff�rents etiquettes utiles et de d�finir leurs propri�t�s
	 * - de les ajouter au conteneur
	 * 
	 * @return ContentPaneEtiquette : Conteneur pour definir les champs de texte de la m�thode proprietesTextField()
	 */
	private JPanel proprietesEtiquette() {
		JPanel ContentPaneEtiquette = new JPanel (new GridLayout(4, 1, 20, 10));

		//1.Etiquette pour le nombre d'�tudiants
		JLabel etiquetteEtudiant = new JLabel("Nombre d'�tudiants :");
		ContentPaneEtiquette.add(etiquetteEtudiant);	

		//2. Etiquette pour le nombre de personnels
		JLabel etiquettePersonnel = new JLabel("Nombre de personnels :");
		ContentPaneEtiquette.add(etiquettePersonnel);		

		//3. Etiquette pour le nombre de visiteurs
		JLabel etiquetteVisiteur = new JLabel("Nombre de visiteurs :");
		ContentPaneEtiquette.add(etiquetteVisiteur);

		//4. Etiquette pour le nombre d'intervanants
		JLabel etiquetteIntervenant = new JLabel("Nombre d'intervenants :");
		ContentPaneEtiquette.add(etiquetteIntervenant);	
		return ContentPaneEtiquette;
	}

	/**
	 * Permet :
	 * - de cr�er le bouton pour valider le nombre de personnes, et de d�finir ses propri�t�s
	 * - de l'ajouter au conteneur
	 * - que l'utilisateur puisse interagir avec
	 * 
	 * @return ContentPaneBouton : Conteneur contenant un bouton, permettant de recuperer les informations rentr�es par l'utilisateur
	 */
	private JPanel proprietesBoutonValide() {
		JPanel ContentPaneBouton = new JPanel (); 

		//1. Bouton pour valider le nombre de personne dans la simulation
		boutonPersonne = new JButton("validez votre choix");
		ContentPaneBouton.add(boutonPersonne);
		boutonPersonne.setPreferredSize(new Dimension(200, 50));
		boutonPersonne.addActionListener(new ActionNombrePersonne());	
		return ContentPaneBouton;
	}

	/**
	 * Cr�ation d'un conteneur interm�diaire pour la fenetre des param�tres 
	 * 
	 * @return ContentPanePersonne : conteneur pour rentrer le nombre de personnes
	 */
	private JPanel conteneurNbPersonne() {
		JPanel ContentPanePersonne = new JPanel ();

		ContentPanePersonne.add(proprietesEtiquette(), BorderLayout.WEST);
		ContentPanePersonne.add(proprietesTextField(), BorderLayout.EAST);
		ContentPanePersonne.add(proprietesBoutonValide(), BorderLayout.SOUTH);
		
		Border EtchedBorderLowered = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		Border EtchedBorderRaised = BorderFactory.createTitledBorder(EtchedBorderLowered, "Nombre de personnes",
				TitledBorder.LEFT,TitledBorder.TOP, new Font("Arial", Font.PLAIN , 13), Color.black);
		ContentPanePersonne.setBorder(EtchedBorderRaised);
		return ContentPanePersonne;
	}

	/**
	 * Permet :
	 * - de cr�er les diff�rents boutons utiles et de d�finir leurs propri�t�s
	 * - de les ajouter au conteneur
	 * - �ventuellement, que l'utilisateur puisse interagir avec
	 * 
	 * @return ContentPaneBouton : conteneur contenant les boutons utiles pour la simulation
	 */
	private JPanel proprietesBoutonUtilisateur() {
		JPanel ContentPaneBouton = new JPanel (new GridLayout(4, 1, 40, 40)); 

		//1. Bouton pour d�marrer la simulation
		boutonSimulation = new JButton("lancement simulation ");
		ContentPaneBouton.add(boutonSimulation);
		boutonSimulation.setEnabled(false);
		boutonSimulation.addActionListener(new ActionBoutonSimulation());
		boutonSimulation.setMnemonic( 'S' );

		//2. Bouton pour activer l'alarme 
		boutonAlarme = new JButton("lancement alarme");
		ContentPaneBouton.add(boutonAlarme);
		boutonAlarme.addActionListener(new ActionAlarme());	
		boutonAlarme.setMnemonic( 'A' );
		boutonAlarme.setEnabled(false);

		//3. Bouton pour r�initialiser la simulation 
		boutonReinitialiser = new JButton("nouvelle simulation");
		ContentPaneBouton.add(boutonReinitialiser);
		boutonReinitialiser.addActionListener(new ActionReinitialisation());		
		boutonReinitialiser.setMnemonic('R');

		//4. Bouton pour mettre en pause/en marche la simulation
		boutonPause = new JButton("EN MARCHE");
		ContentPaneBouton.add(boutonPause);
		boutonPause.addActionListener(new ActionCliquePause());	
		boutonPause.setMnemonic('P');
		boutonPause.setEnabled(false);

		Border EtchedBorderLowered = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		Border EtchedBorderRaised = BorderFactory.createTitledBorder(EtchedBorderLowered, "Boutons",
				TitledBorder.LEFT,TitledBorder.TOP, new Font("Arial", Font.PLAIN , 13), Color.black);
		ContentPaneBouton.setBorder(EtchedBorderRaised);
		return ContentPaneBouton;
	}

	/**
	 * Permet :
	 * - de cr�er les diff�rents boites � cocher pour modifier l'etat des portes et de d�finir leurs propri�t�s
	 * - de les ajouter au conteneur
	 * - que l'utilisateur puisse interagir avec
	 * 
	 * @return ContentPanePorte : conteneur contenant les boites � cocher pour la fermeture/ouverture des portes
	 */
	private JPanel poprietesCheckBox() {
		JPanel ContentPanePorte = new JPanel (new GridLayout(3, 1)); 

		boitePorte1 = new JCheckBox("ouverture porte 1 (haut gauche)");
		ContentPanePorte.add(boitePorte1);		
		boitePorte1.setMnemonic('1');
		boitePorte1.addActionListener(new ActionPorte1());		

		boitePorte2 = new JCheckBox("ouverture porte 2 (bas gauche)");
		ContentPanePorte.add(boitePorte2);		
		boitePorte2.addActionListener(new ActionPorte2());	
		boitePorte2.setMnemonic('2');

		boitePorte3 = new JCheckBox("ouverture porte 3 (droite)");;
		ContentPanePorte.add(boitePorte3);		
		boitePorte3.addActionListener(new ActionPorte3());
		boitePorte3.setMnemonic('3');

		Border EtchedBorderLowered = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		Border EtchedBorderRaised = BorderFactory.createTitledBorder(EtchedBorderLowered, "Reglage des portes",
				TitledBorder.LEFT,TitledBorder.TOP, new Font("Arial", Font.PLAIN , 13), Color.black);
		ContentPanePorte.setBorder(EtchedBorderRaised);
		return ContentPanePorte;
	}


	private JPanel proprietesTitre() {
		/**
		 * Permet :
		 * - de cr�er une �tiquette pour la titre, et de d�finir ses propri�t�s
		 * - de l'ajouter au conteneur
		 * 
		 * @return ContentPaneTitre: conteneur contenant le titre du projet
		 */		
		JPanel ContentPaneTitre = new JPanel ();

		JLabel titre = new JLabel("Simulation de Foule");
		titre.setFont(new Font("Serif", Font.BOLD, 30));
		ContentPaneTitre.add(titre);	

		return ContentPaneTitre; 
	}

	/**
	 * Permet de creer un conteneur intermediaire pour la fenetre des options afin de proposer un affichage graphique construit 
	 * 
	 * @return ContentPaneOptions : conteneur contenant les options de la simualtion
	 */
	private JPanel conteneurOptions() {
		JPanel ContentPaneOptions = new JPanel (new GridLayout(1, 3 ,20, 20));

		ContentPaneOptions.add(conteneurNbPersonne());
		ContentPaneOptions.add(proprietesBoutonUtilisateur());
		ContentPaneOptions.add(poprietesCheckBox());
		return ContentPaneOptions;
	}


	/**
	 * Ouverture d'une fenetre avec les options de la simulation 
	 */
	public void fenetreOption() {
		JFrame fenetreParametre = new JFrame();
		fenetreParametre.setTitle("Options de la simulation");
		fenetreParametre.setSize(850, 400);
		fenetreParametre.setLocationRelativeTo(null);
		fenetreParametre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetreParametre.setVisible(true);

		JPanel ConteneurUtilisateur = (JPanel) fenetreParametre.getContentPane();
		ConteneurUtilisateur.add(proprietesTitre(), BorderLayout.NORTH);
		ConteneurUtilisateur.add(conteneurOptions(), BorderLayout.CENTER);
	}

	public class ActionCliqueParametre implements ActionListener{
		@Override
		/**
		 * Ouverture d'une fenetre avec le foncionnement/aide de la simulation
		 */
		public void actionPerformed(ActionEvent arg0){
			fenetreOption();
		}
	}

	//************************************************************************************************//
	public class ActionCliquePause implements ActionListener{
		@Override
		/**
		 * Permet de mettre en pause ou de r�activer la simulation
		 * (n�c�ssite une action de l'utilisateur)
		 */
		public void actionPerformed(ActionEvent arg0){
			if (!pause) {
				pause = true;
				boutonPause.setText("EN PAUSE");
				menuPause.setText("EN PAUSE");
			}
			else {
				pause = false; 
				boutonPause.setText("EN MARCHE");
				menuPause.setText("EN MARCHE");
			}
		}
	}

	//************************************************************************************************//
	List<Boolean> porteMenuOpenBefore  = Arrays.asList(false, false, false); 	 //etat des portes � l'it�ration pr�c�dente (via le menu)	
	List<Boolean> porteInterfaceOpenBefore = Arrays.asList(false, false, false); //etat des portes � l'it�ration pr�c�dente
	/**
	 * 	Mettre � jour graphiquement l'�tat des portes lorsqu'une d'elle voit son �tat modifi�
	 * 
	 * @param Boite � cocher repr�sentant l'ouverture/fermeture d'une porte 
	 */
	public void changementPorte(JCheckBox boutonPorte, JCheckBoxMenuItem menuPorte) {
		List<Boolean> porteMenuOpen  = Arrays.asList(boiteMenuPorte1.isSelected(), boiteMenuPorte2.isSelected(), boiteMenuPorte3.isSelected());
		List<Boolean> porteInterfaceOpen  = Arrays.asList(boitePorte1.isSelected(), boitePorte2.isSelected(), boitePorte3.isSelected());

		int index = 4; //parce qu'il faut que index soit initialis�, mais pas a 0 ou a 1

		for (int i=0; i<porteInterfaceOpen.size(); i++) {
			if ((porteInterfaceOpen.get(i) != porteInterfaceOpenBefore.get(i))) {        //on regarde si il y a eu une modification depuis le menu ou la fenetre des options
				index = 0;
			}
			else if ((porteMenuOpen.get(i) != porteMenuOpenBefore.get(i))) {
				index = 1;
			}
		}

		for (int i=0; i<porteInterfaceOpen.size(); i++) {          //Synchronisation entre le menu et la fenetre des options 
			if (index == 0) {
				porteMenuOpen.set(i, porteInterfaceOpen.get(i));
				boiteMenuPorte1.setSelected(porteInterfaceOpen.get(0));      
				boiteMenuPorte2.setSelected(porteInterfaceOpen.get(1));
				boiteMenuPorte3.setSelected(porteInterfaceOpen.get(2));
			}
			else {
				porteInterfaceOpen.set(i, porteMenuOpen.get(i));
				boitePorte1.setSelected(porteInterfaceOpen.get(0));
				boitePorte2.setSelected(porteInterfaceOpen.get(1));
				boitePorte3.setSelected(porteInterfaceOpen.get(2));
			}
		}

		for (int i=0; i<porteInterfaceOpen.size(); i++) {                 //on stocke les donnees pr�c�dente pour pouvoir comparer au prochain changement 
			porteInterfaceOpenBefore.set(i, porteInterfaceOpen.get(i));
			porteMenuOpenBefore.set(i, porteInterfaceOpen.get(i));
		}
		repaint(); 
	}

	public class ActionPorte1 implements ActionListener{
		@Override
		/**
		 * Mettre � jour graphiquement l'�tat des portes lorsqu'une d'elle voit son �tat modifi�
		 * (n�c�ssite une action de l'utilisateur)
		 */
		public void actionPerformed(ActionEvent arg0){
			changementPorte(boitePorte1, boiteMenuPorte1);
		}
	}

	public class ActionPorte2 implements ActionListener{
		@Override
		/**
		 * Mettre � jour graphiquement l'�tat des portes lorsqu'une d'elle voit son �tat modifi�
		 * (n�c�ssite une action de l'utilisateur)
		 */
		public void actionPerformed(ActionEvent arg0){
			changementPorte(boitePorte2, boiteMenuPorte2);
		}
	}

	public class ActionPorte3 implements ActionListener{
		@Override
		/**
		 * Mettre � jour graphiquement l'�tat des portes lorsqu'une d'elle voit son �tat modifi�
		 * (n�c�ssite une action de l'utilisateur)
		 */
		public void actionPerformed(ActionEvent arg0){
			changementPorte(boitePorte3, boiteMenuPorte3);
		}
	}

	//************************************************************************************************//
	public class ActionAlarme implements ActionListener{	  		  
		@Override
		/**
		 * Activation de l'alarme (possible une unique fois durant une simulation)
		 * (n�c�ssite une action de l'utilisateur)
		 */
		public void actionPerformed(ActionEvent arg0){
			Sound.play("annexe\\son.wav");
			activationAlarme = true; 
			boutonAlarme.setEnabled(false);
			menuLancementAlarme.setEnabled(false);

		}
	}	

	public static class Sound {
		/**
		 * Lire un fichier wav
		 * 
		 * @param nom du fichier wav � lire 
		 */
		public synchronized static void play(String fileName) {   
			new Thread(new Runnable() {
				public void run() {
					try {
						Clip clip = AudioSystem.getClip();	                    
						AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File(fileName));

						clip.open(inputStream);
						FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
						float range = gainControl.getMaximum() - gainControl.getMinimum();
						float gain = (range *(float) 0.6) + gainControl.getMinimum();
						gainControl.setValue(gain);
						clip.start();
					} 
					catch (Exception e) {
					}
				}
			}).start();
		}
	}

	//************************************************************************************************//
	/**
	 * R�initialisation des diff�rents attributs de la simulation et affichage graphique
	 */
	public void reinitialisation() {
		foule = new Foule(0, 0, 0, 0);
		champEtudiant.setText("10");
		champPersonnel.setText("10");
		champVisiteur.setText("10");
		etiquetteTemps.setText("0"); 
		
		//1. Pour les portes : celle du menu + de la fenetre d'option � false 
		boitePorte1.setSelected(false); 
		boitePorte2.setSelected(false);
		boitePorte3.setSelected(false);
		changementPorte(boitePorte1, boiteMenuPorte1);
		changementPorte(boitePorte2, boiteMenuPorte2);
		changementPorte(boitePorte3, boiteMenuPorte3);

		//2. Boutons via la fenetre des otpions � leur etat initial
		boutonSimulation.setEnabled(false);
		boutonPersonne.setEnabled(true);
		boutonPause.setEnabled(false);
		boutonAlarme.setEnabled(false);

		//3. Dans le menu
		menuLancementSimualtion.setEnabled(false);
		menuValiderPersonne.setEnabled(true);
		menuPause.setEnabled(false);
		menuLancementAlarme.setEnabled(false);
		
		//4. booleens de fonctionnement de la simulation
		activationSimulation = false;  
		initialisation = false;
		activationAlarme = false; 
		reinitialisation  = true;
	}

	public class ActionReinitialisation implements ActionListener {
		@Override
		/**
		 * Reinitialisation de la simulation lorsque l'utilisateur le demande
		 * (n�c�ssite une action de l'utilisateur)
		 */
		public void actionPerformed(ActionEvent arg0){
			reinitialisation(); 
		}
	}

	//************************************************************************************************//

	public class ActionBoutonSimulation implements ActionListener {
		@Override
		/**
		 * Lancement de la simulation
		 * - s'arr�te d�s qu'il n'y a plus personne dans la salle sauf si l'utilisateur r�initilaise la simulation
		 * (n�c�ssite une action de l'utilisateur)
		 */
		public void actionPerformed(ActionEvent arg0) {
			time = 0;
			Thread t = new Thread(new Runnable(){
				public void run() {
					synchronized(foule) {
						boutonAlarme.setEnabled(true);            
						menuLancementAlarme.setEnabled(true);
						
						menuPause.setEnabled(true);
						boutonPause.setEnabled(true);
						
						boutonSimulation.setEnabled(false);
						menuLancementSimualtion.setEnabled(false);
						
						menuValiderPersonne.setEnabled(false);
						boutonPersonne.setEnabled(false);

						initialisation = false; 
						
						while (foule.getListePersonnes().size() != 0 ) { 
							if (reinitialisation) {
								break;
							}
							else if (pause) {
								System.out.print("");
								continue;
							}
							else {
								try {
									Thread.sleep(75);
								} 
								catch (InterruptedException e) {
									e.printStackTrace();
								}
								if (!initialisation && activationSimulation && activationAlarme && !reinitialisation && !pause) {
									DecimalFormat df = new DecimalFormat("###.##");              //format du temps de simulation
									time = time + 1;
									etiquetteTemps.setText(df.format(0.075 * time));
								}
								repaint();
							}
						}
						
					}
				}
			});
			t.start();
		}
	}

	//************************************************************************************************//
	/**
	 * Ouverture d'une fenetre d'erreur
	 */
	public void fenetreErreur() {
		JOptionPane.showMessageDialog(this, "Saisie invalide", "Erreur", JOptionPane.ERROR_MESSAGE);
	}

	public class ActionNombrePersonne implements ActionListener {	  		  
		@Override
		/**
		 * R�cuperation des donn�es rentr�es par l'utilisateur 
		 * Modification de l'activation des boutons afin de permettre le lancement de la simulation
		 */
		public void actionPerformed(ActionEvent arg0){
			String testTxtEtudiant = champEtudiant.getText();     //on recupere les valeurs de l'utilisateur
			String testTxtPersonnel = champPersonnel.getText();
			String testTxtVisiteur = champVisiteur.getText();

			boolean testEtudiant  =  testTxtEtudiant.matches("[+-]?\\d*(\\.\\d+)?");      //on verifie que les donn�es rentr�es sont correctes
			boolean testVisiteur  =  testTxtVisiteur.matches("[+-]?\\d*(\\.\\d+)?");
			boolean testPersonnel =  testTxtPersonnel.matches("[+-]?\\d*(\\.\\d+)?");

			if (!(testEtudiant) || !(testVisiteur) || !(testPersonnel)) {
				fenetreErreur();
				reinitialisation();
			}	
			else {
				nombreEtudiant	= Integer.parseInt(testTxtEtudiant);          // string -> int 
				nombreVisiteur 	= Integer.parseInt(testTxtVisiteur);
				nombrePersonnel = Integer.parseInt(testTxtPersonnel);
				
				activationSimulation = true; 
				menuLancementSimualtion.setEnabled(true);
				
				initialisation 	 = true; 
				reinitialisation = false;
				boutonSimulation.setEnabled(true);
				repaint(); 
			}
		}
	}	


	//__________________________________AFFICHAGE GRAPHIQUE__________________________________//
	//_______________________________________________________________________________________//
	/**
	 *  Affichage d'une personne
	 *  
	 *  @param personne que l'on souhaite afficher sur le graphique
	 *  @param graphique g2d
	 */
	public void affichagePersonne(Personne personne, Graphics2D g2d) {
		g2d.setColor(personne.getColor()); 
		personne.setCircle(new Ellipse2D.Double(personne.getPosition().x, personne.getPosition().y, personne.getTaille(), personne.getTaille()));
		g2d.fill(personne.getCircle());
	}

	/**
	 * Affichage de la foule
	 * 
	 * @param graphique g2d
	 */
	public void affichageFoule(Graphics2D g2d) {
		for (Personne personne : foule.getListePersonnes()) {
			affichagePersonne(personne, g2d);
		}
	}

	/**
	 * Affichage de la foule lors de l'initialisation de la simulation
	 * 
	 *  @param graphique g2d
	 */
	public void initialisationPersonnes(Graphics2D g2d) {
		foule = new Foule(nombreEtudiant, nombrePersonnel, nombreVisiteur, nombreIntervenant);
		affichageFoule(g2d);
	}

	
	List<Boolean> dOpenBefore  = Arrays.asList(null, null, null); //etat des portes � l'it�ration pr�c�dente
	/**
	 *  Affichage de la foule lors de la phase de d�placement lorsque l'alarme est activ�e
	 *  
	 *  @param graphique g2d interface graphique o� tracer les personnes.
	 */
	public void deplacementPersonnes(Graphics2D g2d) {
		List<Coords> dCoords = Arrays.asList(new Coords(0, 165), new Coords(0, 465), new Coords(1190, 315)); 
		List<Boolean> dOpen  = Arrays.asList(boitePorte1.isSelected(), boitePorte2.isSelected(), boitePorte3.isSelected());
	
		//actualisation des d�placements et des portes
		foule.deplacerEvacuation(dOpen, dOpenBefore, dCoords);
		for (int i = 0; i < dOpen.size(); i++) {dOpenBefore.set(i, dOpen.get(i));}

		for (Personne p : foule.getListePersonnes()) {
			affichagePersonne(p, g2d);
		}
	}


	/**
	 * Affichage d'une image  
	 */
	public void affichageAlarme() {
		String imgUrl = "annexe\\alarme.png";
		ImageIcon icone = new ImageIcon(new ImageIcon(imgUrl).getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
		JLabel jlabel = new JLabel(icone, JLabel.CENTER);
		jlabel.setBounds(1150, 30, 50, 50);
		add(jlabel);
	}

	/**
	 * Affichage de la foule lors de la phase de d�placement al�atoire 
	 * 
	 *  @param graphique g2d
	 */
	public void deplacementPersonnesAleatoire(Graphics2D g2d) {
		foule.deplacerRandDestination(foule.destinations);
		affichageFoule(g2d);
	}

	/**
	 * Affichage des obstacles
	 * 
	 * @param graphique g2d
	 */
	public void afficheObstacle(Graphics2D g2d) {
		HashMap<Integer, Color> couleurs = new HashMap<Integer, Color>();
		couleurs.put(0, Color.WHITE);				//vide
		couleurs.put(1,Color.GRAY);					//murs
		couleurs.put(2,Color.orange);				//stands
		couleurs.put(3,new Color(173,52,62));		//porte
		couleurs.put(4,new Color(187, 219,155));	//plantes
		couleurs.put(5,new Color(132, 169,192));	//cafette
		couleurs.put(6,new Color(247, 198,100));	//chaises
		couleurs.put(7,new Color(243, 167,18));		//tables
		couleurs.put(9,new Color(107, 108,118));	//escalier

		RecupObstacle obstacle;
		int largeur = ChargementGrilles.longueur;
		int longueur = ChargementGrilles.largeur; 
		int taille = 10;

		if (boiteMenuPorte1.isSelected()  && boiteMenuPorte2.isSelected()  && boiteMenuPorte3.isSelected() ) {
			obstacle = ChargementGrilles.obsPorte123;
		}
		else if (boiteMenuPorte1.isSelected()  && !boiteMenuPorte2.isSelected() && boiteMenuPorte3.isSelected() ) {
			obstacle = ChargementGrilles.obsPorte13;
		}
		else if (boiteMenuPorte1.isSelected()  && boiteMenuPorte2.isSelected()  && !boiteMenuPorte3.isSelected()) {
			obstacle = ChargementGrilles.obsPorte12;
		}
		else if (!boiteMenuPorte1.isSelected() && !boiteMenuPorte2.isSelected() && boiteMenuPorte3.isSelected() ) {
			obstacle = ChargementGrilles.obsPorte3;
		}
		else if (!boiteMenuPorte1.isSelected() && boiteMenuPorte2.isSelected()  && !boiteMenuPorte3.isSelected()) {
			obstacle = ChargementGrilles.obsPorte2;
		}
		else if (boiteMenuPorte1.isSelected() && !boiteMenuPorte2.isSelected() && !boiteMenuPorte3.isSelected()) {
			obstacle = ChargementGrilles.obsPorte1;
		}
		else if (!boiteMenuPorte1.isSelected() && boiteMenuPorte2.isSelected()  && boiteMenuPorte3.isSelected() ) {
			obstacle = ChargementGrilles.obsPorte23;
		}
		else {
			obstacle = ChargementGrilles.obs;
		}

		for (int i = 0; i < largeur; i++) {
			for (int j = 0; j < longueur; j++) {
				g2d.setColor(couleurs.get(obstacle.grille[i][j]));
				Rectangle2D.Double caseMur = new Rectangle2D.Double(i*taille, (longueur-j-1)*taille, taille, taille);
				g2d.fill(caseMur);
			}
		}
		//trac� du graph
		//graph.tracerGraph(g2d);
		
		//trac� d'un chemin al�atoire
		//PathFinder.tracerTestChemin(g2d);
	}

	/**
	 * Affichage des logos des �coles
	 */
	public void affichageLogo() {
		String imgLogoENSG = "annexe\\logo_ensg.png";
		String imgLogoENPC = "annexe\\logo_enpc.jpg";

		ImageIcon iconeENPC = new ImageIcon(new ImageIcon(imgLogoENPC).getImage().getScaledInstance(200, 110, Image.SCALE_DEFAULT));
		JLabel logoENPC = new JLabel(iconeENPC, JLabel.CENTER);
		logoENPC.setBounds(375, 0, 200, 110);
		
		ImageIcon iconeENSG = new ImageIcon(new ImageIcon(imgLogoENSG).getImage().getScaledInstance(200, 100, Image.SCALE_DEFAULT));
		JLabel logoENSG = new JLabel(iconeENSG, JLabel.CENTER);
		logoENSG.setBounds(380, 530, 200, 100);

		add(logoENPC);
		add(logoENSG);
		}
	
	@Override
	/**
	 * Affichage graphique
	 * 
	 * @param graphique g
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		// 1. Lancement du programme
		if (fenetreOption) {
			afficheObstacle(g2d);
			affichageLogo();
			fenetreOption();
			fenetreOption =  false; 
		}

		// 2. affichage de la grille en fonction des etats (porte ouverte ou ferm�)
		if (!fenetreOption) {
			afficheObstacle(g2d);
		}

		// 3. affichage des personnes
		if (initialisation) {
			initialisationPersonnes(g2d);
		}
		else if (!initialisation && activationSimulation && !activationAlarme && !reinitialisation && !pause) {
			deplacementPersonnesAleatoire(g2d); 
		}
		else if (!initialisation && activationSimulation && activationAlarme && !reinitialisation && !pause) {
			deplacementPersonnes(g2d);
			affichageAlarme();
		}
		else if (pause) {
			affichageFoule(g2d);
		}
	}
}